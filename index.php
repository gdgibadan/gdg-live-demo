<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GDG Ibadan Live Demo</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- 1: Load the Google Identity Toolkit helpers -->
    <?php
      set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ .'/identity-toolkit-client/vendor/google/apiclient/src');
      require_once __DIR__ . '/identity-toolkit-client/vendor/autoload.php';

      $gitkitClient = Gitkit_Client::createFromFile(dirname(__FILE__) . '/gitkit-server-config.json');
      $gitkitUser = $gitkitClient->getUserInRequest();
    ?>
<!-- End modification 1 -->
    <!-- Begin custom code copied from Developer Console -->
    <!-- Note: this is just an example. The html you download from Developer Console will be tailored for your site -->
    <script type="text/javascript" src="//www.gstatic.com/authtoolkit/js/gitkit.js"></script>
    <link type=text/css rel=stylesheet href="//www.gstatic.com/authtoolkit/css/gitkit.css" />
    <script type=text/javascript>
      window.google.identitytoolkit.signInButton(
        '#navbar', // accepts any CSS selector
        {
          widgetUrl: "http://demo.ibadan.gdg.ng/user/login.php",
          signOutUrl: "http://demo.ibadan.gdg.ng/",
          // Optional - Begin the sign-in flow in a popup window
          //popupMode: true,

          // Optional - Begin the sign-in flow immediately on page load.
          //            Note that if this is true, popupMode param is ignored
          //loginFirst: true,

          // Optional - Cookie name (default: gtoken)
          //            NOTE: Also needs to be added to config of ‘widget
          //                  page’. See below
          //cookieName: ‘example_cookie’,
        }
      );
    </script>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">GDG Ibadan</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

            <div id="navbar"></div>
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-offset-4">
                    <!-- 2: Print the user information if a signed in user is present -->
                    <p>
                      <?php if ($gitkitUser) { ?>
                        Welcome back!<br><br>
                        Email: <?= $gitkitUser->getEmail() ?><br>
                        Id: <?= $gitkitUser->getUserId() ?><br>
                        Name: <?= $gitkitUser->getDisplayName() ?><br>
                        Identity provider: <?= $gitkitUser->getProviderId() ?><br>
                      <?php } else { ?>
                        You are not logged in yet.
                      <?php } ?>
                    </p>
                    <!-- End modification 2 -->

                </div>
            </div>
            
        </div>
        
        <hr>

    
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
