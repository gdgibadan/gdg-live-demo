<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GDG Ibadan</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/business-frontpage.css" rel="stylesheet">
    <!-- Begin custom code copied from Developer Console -->
    <!-- Note: this is just an example. The html you download from Developer Console will be tailored for your site -->
    
    <!-- Copy and paste here the "Widget javascript" you downloaded from Developer Console as gitkit-widget.html -->

    <script type="text/javascript" src="//www.gstatic.com/authtoolkit/js/gitkit.js"></script>
    <link type="text/css" rel="stylesheet" href="//www.gstatic.com/authtoolkit/css/gitkit.css" />
    <script type="text/javascript">
      var config = {
          "widgetUrl": "http://demo.ibadan.gdg.ng/user/login.php",
          "signInSuccessUrl": "http://demo.ibadan.gdg.ng/",
          "signOutUrl": "http://demo.ibadan.gdg.ng/",
          "oobActionUrl": "http://demo.ibadan.gdg.ng/",
          "apiKey": "your_api_key",
          "siteName": "this site",
          "signInOptions": ["password","google","yahoo"]          
        };
      // The HTTP POST body should be escaped by the server to prevent XSS
      window.google.identitytoolkit.start(
          '#gitkitWidgetDiv', // accepts any CSS selector
          config,
          JSON.parse('<?php echo json_encode(file_get_contents("php://input")); ?>'));
    </script>

    <!-- End modification -->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">GDG Ibadan</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <hr>

        
    <!-- Include the sign in page widget with the matching 'gitkitWidgetDiv' id -->
    <div id="gitkitWidgetDiv"></div>
    <!-- End identity toolkit widget -->

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>
